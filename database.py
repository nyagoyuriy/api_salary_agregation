from datetime import datetime

import motor.motor_asyncio

from settings import mongoDB

cluster = motor.motor_asyncio.AsyncIOMotorClient(mongoDB)
collection = cluster.Employees_salary.salary

async def get_group_stage(group_type):
    if group_type == 'month':
        fmt ="%Y-%m"
        string_to_full_format = '-01 00:00:00'
    if group_type == 'day':
        fmt = "%Y-%m-%d"
        string_to_full_format = ' 00:00:00'
    if group_type == 'hour':
        fmt = '%Y-%m-%d %H'
        string_to_full_format = ':00:00'

    group_stage = {
        '$group': {
            '_id': {
                '$dateToString': {
                    'format': fmt,
                    'date': "$dt"
                }
            },
            'summary': {'$sum': '$value'}
        }
    }
    return group_stage, string_to_full_format


async def salary_agregation(Item):
    dt_from = Item.dt_from
    dtup_to = Item.dt_upto
    group_type = Item.group_type

    group_stage, full_format_str = await get_group_stage(group_type)
    match_stage = {
        '$match': {
            'dt': {
                '$gte': dt_from,
                '$lte': dtup_to
            }
        }
    }
    sort_stage = {
        "$sort": {"_id": 1}
    }

    cursor = collection.aggregate([match_stage, group_stage, sort_stage])

    my_data_as_list = await cursor.to_list(length=None)
    for entry in my_data_as_list:
        entry['_id'] = datetime.strptime(entry['_id']+full_format_str, "%Y-%m-%d %H:%M:%S")
    return my_data_as_list


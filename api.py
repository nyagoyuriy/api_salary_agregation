from datetime import datetime
from typing import Annotated

from fastapi import FastAPI, Depends
from pydantic import BaseModel

from database import salary_agregation

app = FastAPI()

class Item(BaseModel):
    dt_from: datetime
    dt_upto: datetime
    group_type: str


@app.post("/api")
async def get_data(Item : Item):
    salaries = await salary_agregation(Item)
    response_data = {
        "dataset": [entry['summary'] for entry in salaries],
        "labels": [entry['_id'] for entry in salaries]
    }
    return response_data

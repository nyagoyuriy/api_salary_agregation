import json

import aiohttp

from aiogram import Router
from aiogram.types import Message

router = Router()


@router.message()
async def send_request(msg: Message):
    url = 'http://127.0.0.1:8000/api'
    data = json.loads(msg.text)

    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, json=data) as resp:
            await msg.answer(await resp.text())

